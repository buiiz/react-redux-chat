export type MessageType = {
  avatar: string;
  createdAt: string;
  editedAt: string;
  id: string;
  text: string;
  user: string;
  userId: string;
  isLiked: boolean;
}

export type UserType = {
  id: string;
  name: string;
  avatar: string;
}