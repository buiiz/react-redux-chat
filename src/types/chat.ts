import { MessageType } from "./common";

export interface ChatState {
  chat: {
    messages: MessageType[],
    editMessageId: string | null,
    editModal: boolean,
    preloader: boolean,
    error: boolean,
  }
}

export enum ChatActionTypes {
  FETCH_MESSAGES = 'FETCH_MESSAGES',
  FETCH_MESSAGES_SUCCESS = 'FETCH_MESSAGES_SUCCESS',
  FETCH_MESSAGES_FAILURE = 'FETCH_MESSAGES_FAILURE',
  SEND_MESSAGE = 'SEND_MESSAGE',
  OPEN_EDIT_MESSAGE_MODAL = 'OPEN_EDIT_MESSAGE_MODAL',
  CLOSE_EDIT_MESSAGE_MODAL = 'CLOSE_EDIT_MESSAGE_MODAL',
  LIKE_MESSAGE = 'LIKE_MESSAGE',
  UPDATE_MESSAGE = 'UPDATE_MESSAGE',
  REMOVE_MESSAGE = 'REMOVE_MESSAGE',
}

interface FetchMessagesAction {
  type: ChatActionTypes.FETCH_MESSAGES;
}
interface FetchMessagesSuccessAction {
  type: ChatActionTypes.FETCH_MESSAGES_SUCCESS;
  payload: MessageType[]
}
interface FetchMessagesErrorAction {
  type: ChatActionTypes.FETCH_MESSAGES_FAILURE;
  payload: string;
}

interface SendMessageAction {
  type: ChatActionTypes.SEND_MESSAGE;
  payload: MessageType;
}

interface OpenModalAction {
  type: ChatActionTypes.OPEN_EDIT_MESSAGE_MODAL;
  payload: string;
}

interface CloseModalAction {
  type: ChatActionTypes.CLOSE_EDIT_MESSAGE_MODAL;
}

interface LikeMessageAction {
  type: ChatActionTypes.LIKE_MESSAGE;
  payload: string;
}

interface UpdateMessageAction {
  type: ChatActionTypes.UPDATE_MESSAGE;
  payload: {
    text: string;
    date: string;
  };
}

interface RemoveMessageAction {
  type: ChatActionTypes.REMOVE_MESSAGE;
  payload: string;
}

export type ChatAction = FetchMessagesAction
  | FetchMessagesSuccessAction
  | FetchMessagesErrorAction
  | SendMessageAction
  | OpenModalAction
  | CloseModalAction
  | LikeMessageAction
  | UpdateMessageAction
  | RemoveMessageAction;