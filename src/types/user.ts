import { UserType } from "./common";

export interface UserState {
  user: UserType | null
}

export enum UserActionTypes {
  CREATE_USER = 'CREATE_USER',
}

interface CreateUserAction {
  type: UserActionTypes.CREATE_USER;
  payload: UserType;
}

export type UserAction = CreateUserAction;