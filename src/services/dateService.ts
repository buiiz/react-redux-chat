export const compareDates = (date1: Date, date2: Date) => {
  return date1.getDate() === date2.getDate() &&
    date1.getMonth() === date2.getMonth() &&
    date1.getFullYear() === date2.getFullYear();
}

const isToday = (date: Date) => {
  const today = new Date();
  return compareDates(date, today);
}

const isYesterday = (date: Date) => {
  const today = new Date();
  const yesterday = new Date(today);
  yesterday.setDate(yesterday.getDate() - 1);
  return compareDates(date, yesterday);
}

export const formatDate = (date: Date) => {
  if (isToday(date)) {
    return 'Today';
  }
  if (isYesterday(date)) {
    return 'Yesterday';
  }
  return `${date.toLocaleDateString('en', { weekday: 'long' })
    }, ${date.toLocaleDateString('en', { day: 'numeric' })
    } ${date.toLocaleDateString('en', { month: 'long' })
    }`;
}
