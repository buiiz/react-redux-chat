import { ChatAction, ChatActionTypes, ChatState } from "../../types/chat";

const initialState: ChatState = {
  chat: {
    messages: [],
    editMessageId: null,
    editModal: false,
    preloader: true,
    error: false,
  }
}

export const chatReducer = (state = initialState, action: ChatAction): ChatState => {
  switch (action.type) {
    case ChatActionTypes.FETCH_MESSAGES:
      return {
        ...state, chat: {
          ...state.chat, messages: [], preloader: true, error: false
        }
      }
    case ChatActionTypes.FETCH_MESSAGES_SUCCESS:
      return {
        ...state, chat: {
          ...state.chat, preloader: false, messages: [
            ...action.payload
          ],
        }
      }
    case ChatActionTypes.FETCH_MESSAGES_FAILURE:
      return {
        ...state, chat: {
          ...state.chat, preloader: false, error: true
        }
      }
    case ChatActionTypes.SEND_MESSAGE:
      return {
        ...state, chat: {
          ...state.chat, messages: [
            ...state.chat.messages, action.payload
          ]
        }
      }
    case ChatActionTypes.REMOVE_MESSAGE:
      return {
        ...state, chat: {
          ...state.chat, messages: [
            ...state.chat.messages.filter((message) => message.id !== action.payload)
          ]
        }
      }
    case ChatActionTypes.LIKE_MESSAGE:
      return {
        ...state, chat: {
          ...state.chat, messages: [
            ...state.chat.messages.map((message) => {
              if (message.id === action.payload) {
                return {
                  ...message,
                  isLiked: !message.isLiked
                }
              }
              return message;
            })
          ]
        }
      }
    case ChatActionTypes.OPEN_EDIT_MESSAGE_MODAL:
      return {
        ...state, chat: {
          ...state.chat,
          editModal: true,
          editMessageId: action.payload,
        }
      }
    case ChatActionTypes.CLOSE_EDIT_MESSAGE_MODAL:
      return {
        ...state, chat: {
          ...state.chat,
          editModal: false,
          editMessageId: null,
        }
      }
    case ChatActionTypes.UPDATE_MESSAGE:
      return {
        ...state, chat: {
          ...state.chat, messages: [
            ...state.chat.messages.map((message) => {
              if (message.id === state.chat.editMessageId) {
                return {
                  ...message,
                  text: action.payload.text,
                  editedAt: action.payload.date
                }
              }
              return message;
            })
          ]
        }
      }
    default:
      return state
  }
}