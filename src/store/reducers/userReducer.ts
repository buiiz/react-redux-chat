import { UserAction, UserActionTypes, UserState } from "../../types/user";

const initialState: UserState = {
  user: null
}

export const userReducer = (state = initialState, action: UserAction): UserState => {
  switch (action.type) {
    case UserActionTypes.CREATE_USER:
      return {
        ...state,
        user: {
          ...state.user,
          ...action.payload,
        }
      }
    default:
      return state
  }
}