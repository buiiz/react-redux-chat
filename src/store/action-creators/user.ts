import { nanoid } from 'nanoid';
import { Dispatch } from 'redux';
import { UserType } from "../../types/common";
import { UserAction, UserActionTypes } from '../../types/user';

export const createUser = (name: string) => {
  return (dispatch: Dispatch<UserAction>) => {
    const userId = nanoid()
    const user: UserType = {
      id: userId,
      name: name,
      avatar: `https://avatars.dicebear.com/api/avataaars/${userId}.svg`,
    }
    dispatch({ type: UserActionTypes.CREATE_USER, payload: user })
  }
}