
import * as ChatActionCreators from './chat'
import * as UserActionCreators from './user'


export default {
  ...ChatActionCreators,
  ...UserActionCreators,
}