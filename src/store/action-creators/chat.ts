
import { nanoid } from 'nanoid';
import { Dispatch } from "redux";
import { ChatAction, ChatActionTypes } from "../../types/chat";
import { MessageType, UserType } from "../../types/common";

export const fetchMessages = (url: string) => {
  return async (dispatch: Dispatch<ChatAction>) => {
    try {
      dispatch({ type: ChatActionTypes.FETCH_MESSAGES });
      const response = await fetch(url);
      const data: MessageType[] = await response.json();
      const messages = data.map(message => ({ ...message, isLiked: false }));
      dispatch({ type: ChatActionTypes.FETCH_MESSAGES_SUCCESS, payload: messages });
    } catch (e) {
      dispatch({ type: ChatActionTypes.FETCH_MESSAGES_FAILURE, payload: 'Fetch Messages Error' });
    }
  }
}

export const createMessage = (text: string, user: UserType) => {
  return (dispatch: Dispatch<ChatAction>) => {
    const message: MessageType = {
      id: nanoid(),
      text: text,
      userId: user.id,
      avatar: user.avatar,
      user: user.name,
      createdAt: new Date().toISOString(),
      editedAt: '',
      isLiked: false,
    }
    dispatch({ type: ChatActionTypes.SEND_MESSAGE, payload: message })
  }
}

export const removeMessage = (id: string) => {
  return (dispatch: Dispatch<ChatAction>) => {
    dispatch({ type: ChatActionTypes.REMOVE_MESSAGE, payload: id })
  }
}

export const updateMessage = (text: string) => {
  return (dispatch: Dispatch<ChatAction>) => {
    const date = new Date().toISOString();
    dispatch({ type: ChatActionTypes.UPDATE_MESSAGE, payload: { text, date } })
  }
}

export const likeMessage = (id: string) => {
  return (dispatch: Dispatch<ChatAction>) => {
    dispatch({ type: ChatActionTypes.LIKE_MESSAGE, payload: id })
  }
}

export const openEditModal = (id: string) => {
  return (dispatch: Dispatch<ChatAction>) => {
    dispatch({ type: ChatActionTypes.OPEN_EDIT_MESSAGE_MODAL, payload: id })
  }
}

export const closeEditModal = () => {
  return (dispatch: Dispatch<ChatAction>) => {
    dispatch({ type: ChatActionTypes.CLOSE_EDIT_MESSAGE_MODAL })
  }
}