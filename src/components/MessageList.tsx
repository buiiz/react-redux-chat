import { FC, Fragment, useEffect, useRef } from 'react'
import { useTypedSelector } from '../hooks/useTypedSelector'
import { compareDates, formatDate } from '../services/dateService'
import { Message } from './Message'
import { OwnMessage } from './OwnMessage'

export const MessageList: FC = () => {
  const { user } = useTypedSelector(state => state.user);
  const { chat: { messages } } = useTypedSelector(state => state.chat);

  const messagesEndRef = useRef<HTMLDivElement>(null)

  const scrollToBottom = () => {
    if (messagesEndRef.current) {
      messagesEndRef.current.scrollIntoView({ behavior: "smooth" })
    }
  }

  useEffect(scrollToBottom, [messages.length]);

  return (
    <div className="message-list px-1 flex-grow overflow-y-auto">
      {messages.map((message, index) => (
        <Fragment key={message.id}>
          {(!compareDates(new Date(message.createdAt), new Date(messages[index - 1]?.createdAt))) && (
            <div className="messages-divider flex justify-center">
              <span className="px-3 py-1.5 rounded-full bg-opacity-20 bg-gray-400">
                {formatDate(new Date(message.createdAt))}
              </span>
            </div>
          )}
          {(user && user.id === message.userId) ? (
            <OwnMessage message={message} />
          ) : (
            <Message message={message} />
          )}
        </Fragment>
      ))}
      <div ref={messagesEndRef} />
    </div>
  )
}
