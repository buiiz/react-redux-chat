import { ChangeEvent, FormEvent, useState } from "react";
import { useActions } from "../hooks/useActions";
import { useTypedSelector } from "../hooks/useTypedSelector";

export const EditModal = () => {
  const { chat: { messages, editMessageId, editModal } } = useTypedSelector(state => state.chat);
  const editedMessage = messages.find(message => message.id === editMessageId)
  const { closeEditModal, updateMessage } = useActions();
  const [text, setText] = useState<string>(editedMessage?.text!)

  const handleSubmit = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    updateMessage(text);
    closeEditModal();
  }

  return (
    <div
      className={`edit-message-modal ${editModal ? 'edit-message-modal' : ''} fixed text-gray-500 flex items-center justify-center overflow-auto z-50 bg-black bg-opacity-40 left-0 right-0 top-0 bottom-0`}
    >
      <form
        onSubmit={handleSubmit}
        className="bg-white rounded-xl shadow-2xl p-6 max-w-3xl sm:w-10/12 mx-1 sm:mx-10"
      >
        <h2 className="font-bold block text-2xl mb-6">
          Edit message
        </h2>

        <textarea
          autoFocus
          value={text}
          onChange={(e: ChangeEvent<HTMLTextAreaElement>) => setText(e.currentTarget.value)}
          className="edit-message-input h-full w-full rounded text-gray-900 border border-gray-300 focus:border-indigo-500 focus:ring-2 text-base outline-none py-1 px-3 leading-7 transition-colors duration-200 ease-in-out"
        />

        <div className="text-right space-x-5 mt-6">
          <button
            type="button"
            onClick={closeEditModal}
            className="edit-message-close px-4 py-2 text-sm bg-white rounded-xl border transition-colors duration-150 ease-linear border-gray-200 text-gray-500 focus:outline-none focus:ring-0 font-bold hover:bg-gray-50 focus:bg-indigo-50 focus:text-indigo"
          >
            Cancel
          </button>
          <button
            type="submit"
            className="edit-message-button px-4 py-2 text-sm bg-blue-500 rounded-xl border transition-colors duration-150 ease-linear border-gray-200 text-white focus:outline-none focus:ring-0 font-bold hover:bg-blue-700 focus:bg-blue-700 focus:text-indigo"
          >
            Update
          </button>
        </div>
      </form>
    </div >
  )
}
