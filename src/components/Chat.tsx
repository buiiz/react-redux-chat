import { FC, useEffect } from 'react'
import { useActions } from '../hooks/useActions'
import { useTypedSelector } from '../hooks/useTypedSelector'
import { EditModal } from './EditModal'
import { Header } from './Header'
import { MessageInput } from './MessageInput'
import { MessageList } from './MessageList'
import { Preloader } from './Preloader'

interface Props {
  url: string;
}

export const Chat: FC<Props> = ({ url }) => {
  const { chat: { messages, preloader, editModal } } = useTypedSelector(state => state.chat);
  const { fetchMessages, createUser } = useActions();

  useEffect(() => {
    createUser('John Doe');
  }, [])

  useEffect(() => {
    fetchMessages(url)
  }, [url])

  return (
    <div className="chat mx-auto p-2 sm:p-4 max-w-4xl w-full h-screen flex flex-col gap-2 sm:gap-4 overflow-hidden border-gray-300 border shadow-2xl rounded bg-gradient-to-r from-blue-100 to-pink-100">
      {preloader ?
        (<>
          <Preloader />
        </>) :
        (<>
          <Header messages={messages} />
          <MessageList />
          <MessageInput />
        </>)
      }
      {editModal && <EditModal />}
    </div>
  )
}
