import { Chat } from "./src/components/Chat";
import { rootReducer } from "./src/store/reducers/";

export default {
  Chat,
  rootReducer,
};
